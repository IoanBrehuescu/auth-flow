# Auth - Flo


## 1. **Start/Authentifizierung**:

Der Prozess beginnt mit einem Authentifizierungsversuch.

## 2. **Überprüfung der Registrierung (Ist registriert?)**:

- Wenn der Benutzer nicht registriert ist, wird er zur **Registrierung (Registrierung)** weitergeleitet.
- Wenn der Benutzer bereits registriert ist, wird er zum **Login** weitergeleitet.

## 3. **Registrierung**:

Während der Registrierung gibt der Benutzer eine E-Mail-Adresse und ein Passwort ein,
das zur Bestätigung wiederholt wird. Das Passwort wird dann verschlüsselt(gehasht) und in der Datenbank gespeichert.

## 4. **Login**: 

Der Benutzer gibt seine E-Mail und sein Passwort ein, um sich anzumelden.

## 5. **Backend-Validierung**: 

Das Backend überprüft die eingegebenen Anmeldeinformationen. Wenn sie korrekt sind:

## 6. **Erzeugung einer einzigartigen Session-ID**: 

Das System generiert eine einzigartige Session-ID für den Benutzer.

## 7. **Setzen eines Cookies**:

Die Session-ID wird als Cookie zurück an den Browser des Benutzers gesendet.
 Dieses Cookie wird verwendet, um den authentifizierten Zustand des Benutzers für nachfolgende Anfragen an den Server aufrechtzuerhalten.

## 8. **Eingeloggt**:

Der Benutzer ist nun im System angemeldet.

## 9. **Ende**:

Der Authentifizierungsprozess ist nun abgeschlossen, und der Benutzer kann auf geschützte Ressourcen zugreifen.

## 10. **Fehlermeldung Passwort vergessen?**:
Wenn der Benutzer sein Passwort vergessen hat, kann er den Prozess zum Zurücksetzen des Passworts initiieren.

## 11. **Passwort zurücksetzen**: 
Der Benutzer wird aufgefordert, seine E-Mail-Adresse einzugeben, um den Prozess zum Zurücksetzen des Passworts zu beginnen.

## 12. **E-Mail - Link zum Zurücksetzen des Passworts senden**: 
Ein Link zum Zurücksetzen des Passworts wird an die E-Mail-Adresse des Benutzers gesendet.

## 13. **E-Mail wurde gesendet**:
Der Benutzer wird darüber informiert, dass die E-Mail gesendet wurde.

## 14. **Öffnen der HTML-Seite zum Zurücksetzen des Passworts**:
Der Benutzer öffnet die HTML-Seite, um sein Passwort zurückzusetzen.

## 15. **Neues Passwort und Passwortwiederholung**: 
Der Benutzer gibt ein neues Passwort ein und wiederholt es zur Bestätigung.

## 16. **Datenbank **:
Alle diese Aktionen werden in der Datenbank widergespiegelt, die die Registrierungsdaten speichert, Anmeldeinformationen validiert und Passwörter aktualisiert.

## 17. **Anmeldeprozess mit dem neuen Passwort wiederholen**:
Nach dem Zurücksetzen und Validieren des neuen Passworts meldet sich der Benutzer erneut mit dem neuen Passwort an,
und der Authentifizierungsprozess beginnt erneut bei Schritt 4.

## Antworten auf die Fragen

  Wird ein JWT benutzt? - ** 7 **
  Funktioniert es mit einer Session‐ID oder einem Cookie? -  ** 6 **
  Wann wird das Passwort wie verschlüsselt? - ** 3 **
  Gibt es eine Datenbank? - ** 16 ** , ** 3 **